
export class Value {
  value: string;
  propertyIds: Array<string> = new Array<string>();
  filtered: boolean;
}
