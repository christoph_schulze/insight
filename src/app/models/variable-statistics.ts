
export class VariableStatistics {
  variableId: string;
  propertyIds: Array<string> = new Array<string>();
}
