import { PropertyStatistics } from './property-statistics';
import { NameValue } from './name-value';

export class Property {
  id: string;
  lhs: NameValue[];
  rhs: NameValue[];
  filtered: boolean = false;
  statistics: PropertyStatistics = new PropertyStatistics();

}
