
export class PropertyFile {
  fileName: string;
  propertyIds: Array<string> = new Array<string>();
  active: boolean = true;
}
