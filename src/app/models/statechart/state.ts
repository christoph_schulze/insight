import {NameValue} from '../name-value';

export class State{
  id: NameValue[] = [];
  name: string = '';
  alternativeId: NameValue[] = [];
  incomingIds: string[] = [];
  outgoingIds: string[] = [];

  printId(): string{
    let idString = "";

    this.id.forEach(nameValue => {
      idString += nameValue.name + nameValue.value;
    });

    return idString;
  }

}

