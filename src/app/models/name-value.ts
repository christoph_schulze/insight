export class NameValue {
  name: string;
  value: string;
  inequality: string;

  equals(toCompare: NameValue): boolean{
    if(this.name == toCompare.name && this.value == toCompare.value && this.inequality == toCompare.inequality){
      return true;
    }
    return false;
  }
}
