import { Injectable } from '@angular/core';

import {DataService} from './data.service';
import { PropertyFile } from '../models/property-file';

@Injectable()
export class ComparisonService {
  public onlyInFirst: Set<string> = new Set();
  public onlyInSecond: Set<string> = new Set();
  public inBoth: Set<string> = new Set();
  public firstFile: PropertyFile = null;
  public secondFile: PropertyFile = null;

  constructor(public _dataService: DataService) { }

  compare(firstFile: PropertyFile, secondFile: PropertyFile){
    this.firstFile = firstFile;
    this.secondFile = secondFile;
    this.onlyInFirst.clear();
    this.onlyInSecond.clear();
    this.inBoth.clear();

    firstFile.propertyIds.forEach(id => {
      if(secondFile.propertyIds.includes(id)){
        this.inBoth.add(id);
      }else {
        this.onlyInFirst.add(id);
      }
    });
    secondFile.propertyIds.forEach(id => {
      if(!firstFile.propertyIds.includes(id)){
        this.onlyInSecond.add(id);
      }
    });
  }
}
