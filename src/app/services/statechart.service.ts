import { Injectable } from '@angular/core';

import { DataService } from './data.service';
import { Variable } from '../models/variable';
import { Property } from '../models/property';
import { NameValue } from '../models/name-value';
import { VariablePair } from '../models/variable-pair';
import { StateChart } from '../models/statechart/statechart';
import { State } from '../models/statechart/state';
import { Transition } from '../models/statechart/transition';
import { StatechartElement } from '../models/statechart/statechart-element';

@Injectable()
export class StatechartService {
  private transitionId = 0;

  constructor(private _dataService: DataService) { }

  private pairs: VariablePair[];


  /**
   * Create a statemachine in the DOT format
   * @param pairs , the variable pairs that make up the state space of the state machine
   * @param ignores , variables that should not be considered in the transitions
   */
  createStateChart(pairs: VariablePair[], ignores: String[]): string {
    this.pairs = pairs;
    let statechart = new StateChart();

    //Identify all properties that have at least one of the variables
    let propertyIds: Set<String> = this.getProperties(pairs, ignores);

    //Create the state space
    this.createStates(pairs, propertyIds, statechart);

    //Create the transitions
    this.createTransitions(pairs, propertyIds, statechart);

    //Return DOT string
    return statechart.printDOT();
  }

  /**
   * Create the state space of the state machine
   * @param pairs , the variable pairs that make up the state space
   * @param properties , the properties that entail the state space
   * @param stateChart , the state machine data structure
   */
  private createStates(pairs: VariablePair[], properties: Set<String>, stateChart: StateChart) {

    properties.forEach(propertyId => {
      let property = this._dataService.propertyMap.get(propertyId);
      this.createStatesFromProperty(property, pairs, stateChart);
    });

  }

  /**
   * Analyze lhs and rhs of property and create the corresponding states for the state space
   * if they do not exist yet. E.g. for stateVar1Old=1 & otherVar=1 -> stateVar2=2 it would
   * create the two states stateVar1=1 and statevar2=2
   * @param property
   * @param pairs
   * @param stateChart
   */
  private createStatesFromProperty(property: Property, pairs: VariablePair[], stateChart: StateChart){
    //Create state LHS
    this.createStateFromProperty(property.lhs, pairs, stateChart);
    //Create state RHS
    this.createStateFromProperty(property.rhs, pairs, stateChart);
  }

  /**
   * Analyzes either the LHS or RHS of a property to create its corresponding state space
   * @param property
   * @param pairs
   * @param stateChart
   */
  private createStateFromProperty(property: NameValue[], pairs: VariablePair[], stateChart: StateChart){
    //Get all name values that are both in the porperties LHS and in the list of observed variable pairs
    let nameValues = this.getNameValuesEqualPairs(property, pairs);
    /**
     * If there are no name values that are both in the properties and observed variables there will be no state
     */
    if(nameValues.length != 0){
      //Create a state from the nameValue list
      let state = this.createStateFromNameValues(nameValues);
      /**
       * Check if the state does not exist yet and then add it to the state machine
       */
      if(!this.stateExists(state, stateChart)){
        //Get alternative ID for the state (based on the different names of the observed variables)
        let nameValuesAlternative = this.getNameValuesEqualPairsAlternative(property, pairs);
        state.alternativeId = nameValuesAlternative;
        //Add state to state machine
        stateChart.states.push(state);
      }
    }
  }

  /**
   * Check the list of name values for variables that are also in the list of variable pairs and put
   * all of them in a new list
   * @param nameValuesProperty , the list of name values
   * @param pairs , the list ov variable pairs
   */
  private getNameValuesEqualPairs(nameValuesProperty: NameValue[], pairs: VariablePair[]): NameValue[]{
    let nameValues: NameValue[] = [];

    /**
     * Loop over all name values
     */
    for(let nameValue of nameValuesProperty){
      //Check if the name value has a corresponding variable in the list of variable pairs and get its name
      let name = this.getName(nameValue.name, pairs);
      /**
       * If the name value has a corresponding variable add it to the result list
       */
      if(name != null){
        let newNameValue = new NameValue();
        newNameValue.name = name;
        newNameValue.value = nameValue.value;
        nameValues.push(newNameValue);
      }
    }

    return nameValues;
  }

  /**
   * Similar function than getNameValuesEqualPairs but it creates a name value list with the alternative names
   * of the variables
   * @param nameValuesProperty , the list of name values
   * @param pairs , the list ov variable pairs
   */
  private getNameValuesEqualPairsAlternative(nameValuesProperty: NameValue[], pairs: VariablePair[]): NameValue[]{
    let nameValues: NameValue[] = [];

    /**
     * Loop over all name values
     */
    for(let nameValue of nameValuesProperty){
      //Check if the name value has a corresponding variable in the list of variable pairs and get its alternative name
      let name = this.getNameAlternative(nameValue.name, pairs);
      /**
       * If the name value has a corresponding variable add it to the result list
       */
      if(name != null){
        let newNameValue = new NameValue();
        newNameValue.name = name;
        newNameValue.value = nameValue.value;
        nameValues.push(newNameValue);
      }
    }

    return nameValues;
  }

  /**
   * Check if the state already exists in the state chart. Equivalence is determined based on the ID of the state.
   * @param state , the state to search
   * @param stateChart , the statechart to search
   */
  private stateExists(state: State, stateChart: StateChart): boolean{
    for(let existingState of stateChart.states){
      if(this.statesAreEqual(existingState, state)){
        return true;
      }
    }

    return false;
  }

  /**
   * Check if two states are equal based on their id's
   * @param state
   * @param toCompare
   */
  private statesAreEqual(state: State, toCompare: State): boolean{
    let found = true;

    /**
     * If the id's are of different length the states cannot be the same
     */
    if(state.id.length != toCompare.id.length){
      return false;
    }

    /**
     * Loop over the id list of one state and check that it has a corresponding id in the other state
     */
    for(let nameValue of state.id){
      if(!found){
        return false;
      }
      found = false;
      for(let toCompareNameValue of toCompare.id){
        if(nameValue.equals(toCompareNameValue)){
          found = true;
          break;
        }
      }
    }
    return found;
  }

  /**
   * Create a state based on a list of name values
   * @param nameValues
   */
  private createStateFromNameValues(nameValues: NameValue[] ): State{
    let state = new State();
    //The list of name values will be used as the id of the state
    state.id = state.id.concat(nameValues);

    /**
     * Create the state's name by concatenating all the name values into one string seperated
     * by line breaks
     */
    let stateName = '';
    let first = true;
    for(let nameValue of nameValues){
      if(first){
        first = false;
        stateName += nameValue.name + ' = ' + nameValue.value;
      }else{
        stateName += '\n' + nameValue.name + ' = ' + nameValue.value;
      }
    }

    state.name = stateName;

    return state;
  }

  /**
   * Get the name of the variable. The name is the first variable of the variable pair
   * @param name
   * @param pairs
   */
  private getName(name: string, pairs: VariablePair[]): string{
    for(let pair of pairs){
      if(pair.firstVariable == name || pair.secondVariable == name){
        return pair.firstVariable;
      }
    }
    return null;
  }

  /**
   * Get the alternative name of the variable. The alternative name is the first variable of the variable pair
   * @param name
   * @param pairs
   */
  private getNameAlternative(name: string, pairs: VariablePair[]): string{
    for(let pair of pairs){
      if(pair.firstVariable == name || pair.secondVariable == name){
        return pair.secondVariable;
      }
    }
    return null;
  }

  /**
   * Create the transitions of the state machines from the properties
   * @param pairs
   * @param properties
   * @param stateChart
   */
  private createTransitions(pairs: VariablePair[], properties: Set<String>, stateChart: StateChart) {
    /**
     * Loop over all property id's
     */
    properties.forEach(propertyId => {
      //Retrieve property from propertyId
      let property = this._dataService.propertyMap.get(propertyId);

      /**
       * If property exists, use it to create transition
       */
      if (property != undefined) {
        this.createTransition(pairs, property, stateChart);
      }
    });
  }

  /**
   * Finds all properties that have any of the variables in the variable pair array.
   * Filters out proerties that have variables in the ignore list
   * @param pairs , the pairs of variables (n, n-1) that are observed
   * @param ignores , variables that should be ignored
   */
  private getProperties(pairs: VariablePair[], ignores: String[]): Set<String>{
    let propertiesFirst: string[] = [];
    let propertiesSecond: string[] = [];
    let finalProperties: Set<String> = new Set<String>();

    /**
     * For each variables in the pairs get their associated properties
     * and add them to the properties set
     */
    pairs.forEach(pair => {
      let firstVariable = this._dataService.variableMap.get(pair.firstVariable);
      let secondVariable = this._dataService.variableMap.get(pair.secondVariable);

      /**
       * Get all property id's that are related to the first variable
       * but do not contain variables on the ignore list
       */
      propertiesFirst = firstVariable.propertyIds.filter(propertyId => {
        let property = this._dataService.propertyMap.get(propertyId);
        return !this.containsVariable(property, ignores);
      });

      /**
       * Get all property id's that are related to the second variable
       * but do not contain variables on the ignore list
       */
      propertiesSecond = secondVariable.propertyIds.filter(propertyId => {
        let property = this._dataService.propertyMap.get(propertyId);
        return !this.containsVariable(property, ignores);
      });

      //Add the properties to the result set
      this.concatSets(finalProperties, propertiesFirst, propertiesSecond);
    });
    return finalProperties;
  }

  /**
   * Check if a property contains an ignored variable
   * @param property
   * @param ignores
   */
  containsVariable(property: Property, ignores: String[]): boolean {
    /**
     * Check LHS of the property
     */
    for (let nameValue of property.lhs) {
      if (ignores.includes(nameValue.name)) {
        return true;
      }
    }

    /**
     * Check RHS of the property
     */
    for (let nameValue of property.rhs) {
      if (ignores.includes(nameValue.name)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Add a list of iterables to a set
   * @param set
   * @param iterables
   */
  concatSets(set, ...iterables) {
    for (const iterable of iterables) {
      for (const item of iterable) {
        set.add(item);
      }
    }
  }

  /**
   * Create a transition based on a property
   * @param pairs the variable pairs that make up the state space
   * @param property the property that is the basis of the transition
   * @param statechart the statechart where we add the transiton
   */
  createTransition(pairs: VariablePair[], property: Property, statechart: StateChart): void {
    //Identify the target state of the future transition, based on the rhs of the property
    let targetState = this.getState(property.rhs, pairs, statechart);

    /**
     * If the property doesn't correspond to a target state there cannot be a valid transition
     */
    if (targetState == null) {
      return;
    }

    //Identify the source state of the future transiton, based on the lhs of the property
    let sourceState = this.getState(property.lhs, pairs, statechart);

    /**
     * If both a source and target state exist, create a transition between them
     */
    if (sourceState != null) {
      this.addTransition(property, sourceState, targetState, statechart);

    }
    /**
     * Only a target and no source state exist. This means that the transition
     * has to be added for all states in the model with to the target state
     */
    else {
      for (let sourceState of statechart.states) {
        this.addTransition(property, sourceState, targetState, statechart);
      }
    }
  }

  /**
   * Add a transition between two states
   * @param property
   * @param sourceState
   * @param targetState
   * @param statechart
   */
  private addTransition(property: Property, sourceState: State, targetState : State, statechart: StateChart){
    let transition = new Transition();
      //The name/label of the transiton will be the LHS of the property minus any state variables
      let name = this.getTransitionNameByRemovingVar(property, sourceState);
      transition.id = "" + this.transitionId++;

      transition.srcId = sourceState.printId();
      transition.targetId = targetState.printId();

      //Add transition id's to the source and target states
      sourceState.outgoingIds.push(transition.id);
      targetState.incomingIds.push(transition.id);

      //Check if there already exists a transition between the two states
      let existingTransition = this.getTransitionWithSameSrcTarget(statechart, transition.srcId, transition.targetId);

      /**
       * If there is no existing transition then add it to the statechart
       */
      if (existingTransition == null) {
        transition.name.push(name);
        statechart.transitions.push(transition);
      }
      else {
        //If a transition already exists add the new label to the transition
        existingTransition.name.push(name);
      }
  }

  /**
   * Check if there already exists a transition between the sourceId and targetId
   * @param stateChart the statechart to search
   * @param srcId the sourceId to check
   * @param targetId the targetId to check
   */
  getTransitionWithSameSrcTarget(stateChart: StateChart, srcId: string, targetId: string): Transition {
    /**
     * Loop over all existing transitions int he model
     */
    for (let existingTransition of stateChart.transitions) {
      /*
      * Check if the existing transition corresponds to the srcId and targetId
      */
      if (existingTransition.srcId == srcId && existingTransition.targetId == targetId) {
        return existingTransition;
      }
    }
    return null;
  }

  /**
   * Get the name of a transition by removing the state variables from the property list
   * @param property
   * @param state
   */
  getTransitionNameByRemovingVar(property: Property, state: State): string {
    //The resulting name string
    let result = "";
    let first = true;
    /**
     * For each element in the LHS of the property
     */
    for (let element of property.lhs) {
      /**
       * Check if the elements variable exists in the state's alternative id list
       */
      let found = state.alternativeId.some(id => {
        if (id.name == element.name && id.value == element.value) {
          return true;
        }
      });

      /**
       * If the element does not exist in the state's alternative id list,
       * then it is part of the name
       */
      if (!found) {
        if (first) {
          first = false;
          result += element.name + "=" + element.value;
        } else {
          result += " ^ " + element.name + "=" + element.value;
        }
      }
    }

    return result;
  }

  /**
   * Identify a state in a statechart using a list of nameValues
   * @param nameValues , the nameValues of the search
   * @param statechart , the statechart we are searching
   */
  getStateByNameValues(nameValues: NameValue[], statechart: StateChart): State {
    let foundState: State = null;

     for (let state of statechart.states) {
      if (this.hasSameIds(nameValues, state)) {
        return state;
      }
    }

    return null;
  }


  /**
   * Identify a state in a statechart using a list of nameValues and variable pairs
   * @param nameValues
   * @param pairs
   * @param statechart
   */
  getState(nameValues: NameValue[], pairs: VariablePair[], statechart: StateChart): State {
    let nameValuesEqualPairs = this.getNameValuesEqualPairs(nameValues, pairs);
    let foundState = this.getStateByNameValues(nameValuesEqualPairs, statechart);
    return foundState;
  }

  /**
   * Check if a list of nameValues corresponds to a state's id list
   * @param nameValues
   * @param state
   */
  hasSameIds(nameValues: NameValue[], state: State): boolean {
    let found = true;

    if(nameValues.length != state.id.length){
      return false;
    }

    for(let nameValue of state.id){
      if(!found){
        return false;
      }
      found = false;
      for(let toCompareNameValue of nameValues){
        if(nameValue.equals(toCompareNameValue)){
          found = true;
          break;
        }
      }
    }
    return found;
  }

}
