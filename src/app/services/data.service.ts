import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Property } from '../models/property';
import { Variable } from '../models/Variable';
import { Value } from '../models/Value';
import { PropertyFile } from '../models/property-file';
import { NameValue } from '../models/name-value';

@Injectable()
export class DataService {
  propertyMap: Map<String, Property> = new Map<String, Property>();
  variableMap: Map<String, Variable> = new Map<String, Variable>();
  files: Array<PropertyFile> = new Array<PropertyFile>();

  currentProperties: string[] = [];

  reset(){
    this.propertyMap.clear();
    this.variableMap.clear();
    this.files.splice(0,this.files.length)
    this.currentProperties.splice(0,this.currentProperties.length);
  }

  resetProperties() {
    this.currentProperties.length = 0;
    this.propertyMap.forEach(property => {
      this.currentProperties.push(property.id);
    });
  }

  showTheseProperties(propertyIds: Set<string>){
    this.currentProperties.length = 0;
    propertyIds.forEach(id => {
      this.currentProperties.push(id);
    });

  }

  onlyShowPropertiesBetween(inputName: string, outputName: string){
    this.currentProperties.length = 0;
    this.propertyMap.forEach(property => {
      let found = true;
      found = property.lhs.some(variable => {
        if(variable.name == inputName){
          return true;
        }
      });
      if(!found){
        return;
      }
      found = property.rhs.some(variable => {
        if(variable.name == outputName){
          return true;
        }
      });
      if(found){
        this.currentProperties.push(property.id);
      }
    });
  }

  addProperty(lhs: NameValue[], rhs: NameValue[], fileID: string) {
    // Create property id, to see if property already exists
    let propertyID  = this.stringify(lhs) + '' + this.stringify(rhs);

    let property = <Property> this.propertyMap.get(propertyID);

    // if property doesn't exist create a new instance and add it to the map
    if (property == null) {
      property = new Property();
      property.lhs = lhs;
      property.rhs = rhs;
      property.filtered = false;
      property.id = propertyID;

      // add property to the map
      this.propertyMap.set(propertyID, property);
    }
    property.statistics.fileIds.push(fileID);
    // update the variables
    this.updateVariables(property);

    for (let file of this.files) {
      if (file.fileName === fileID) {
        file.propertyIds.push(property.id);
        return;
      }
    }
  }

  updateVariables(property: Property) {
    property.lhs.forEach(variable => {
      this.updateVariable(variable, property.id, true);
    });

    property.rhs.forEach(variable => {
      this.updateVariable(variable, property.id, false);
    });
  }

  updateVariable(namedValue: NameValue, propertyId: string, isLHS: boolean) {
    let variable = this.variableMap.get(namedValue.name);

    if (variable == null) {
      variable = new Variable();
      variable.isLHS = isLHS;

      variable.filtered = false;
      variable.name = namedValue.name;

      this.variableMap.set(variable.name, variable);
    }
    variable.propertyIds.push(propertyId);
    this.updateValue(variable, namedValue.value, propertyId);
  }

  updateValue(variable: Variable, valueString: string, propertyId: string) {
    let value = variable.valuesMap.get(valueString);

    if (value == null) {
      value = new Value();
      value.filtered = false;
      value.value = valueString;

      variable.valuesMap.set(value.value, value);
    }
    value.propertyIds.push(propertyId);
  }

  removePropertyReferences(file: PropertyFile) {
    file.propertyIds.forEach(propertyID => {
      this.removePropertyReference(propertyID, file.fileName);
    });
    file.propertyIds.length = 0;
  }

  removePropertyReference(propertyID: string, fileName: string) {
      let property = this.propertyMap.get(propertyID);
      let index = property.statistics.fileIds.indexOf(fileName, 0);
      if (index > -1) {
        property.statistics.fileIds.splice(index, 1);
      }
      if (property.statistics.fileIds.length === 0) {
        this.removeVariableReferences(property);
        this.propertyMap.delete(propertyID);
      }
  }

  removeVariableReferences(property: Property) {
    property.lhs.forEach(variable => {
      this.removeVariableReference(variable, property.id);
    });

    property.rhs.forEach(variable => {
      this.removeVariableReference(variable, property.id);
    });
  }

  removeVariableReference(variableID: NameValue, propertyID: string) {
    let variable = this.variableMap.get(variableID.name);

    // Remove Value reference
    let value = variable.valuesMap.get(variableID.value);

    let index = value.propertyIds.indexOf(propertyID, 0);

    if (index > -1) {
      value.propertyIds.splice(index, 1);
    }

    if (value.propertyIds.length === 0) {
      variable.valuesMap.delete(variableID.value);
    }

    // Remove Variable reference
    if (variable.valuesMap.size === 0) {
      this.variableMap.delete(variableID.name);
    }

  }

  stringify(nameValues: NameValue[]): string {
    let stringified = '';
    nameValues.forEach(nameValue => {
      stringified = stringified + nameValue.name + nameValue.value;
    });
    return stringified;
  }

}
