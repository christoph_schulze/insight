import { Component, OnInit } from '@angular/core';
import { MdSnackBar, MdSnackBarConfig} from '@angular/material';

import { InitializationService } from '../../services/initialization.service';
import { DataService } from '../../services/data.service';

@Component({
  moduleId: module.id,
  selector: 'my-start-screen',
  templateUrl: 'start-screen.component.html',
  styleUrls: ['start-screen.component.css'],
})

export class StartScreenComponent implements OnInit {
  constructor(public _initializationService: InitializationService, public _dataService: DataService, public snackBar: MdSnackBar) { }

  ngOnInit() {

  }

  loadHeart(){
    this._dataService.reset();
    this._initializationService.loadHeart();
    let config = new MdSnackBarConfig();
    config.duration = 3000;
    this.snackBar.open('Data loaded successfully', null, config);
  }

  infoHeart(){
    window.open('https://bitbucket.org/christoph_schulze/specificationinspector/wiki/HeartExample');
    //window.location.href = ;
  }

  loadPacemaker(){
    this._dataService.reset();
    this._initializationService.loadPacemaker();
    let config = new MdSnackBarConfig();
    config.duration = 3000;
    this.snackBar.open('Data loaded successfully', null, config);
  }

  infoPacemaker(){
    window.open('https://bitbucket.org/christoph_schulze/specificationinspector/wiki/PacemakerExample');
    //window.location.href = ;
  }

  loadCruise(){
    this._dataService.reset();
    this._initializationService.loadCruise();
    let config = new MdSnackBarConfig();
    config.duration = 3000;
    this.snackBar.open('Data loaded successfully', null, config);
  }

  loadTest(){
    this._dataService.reset();
    this._initializationService.loadTest();
    let config = new MdSnackBarConfig();
    config.duration = 3000;
    this.snackBar.open('Data loaded successfully', null, config);
  }

  infoCruise(){
    window.open('https://bitbucket.org/christoph_schulze/specificationinspector/wiki/CruiseExample');
    //window.location.href = ;
  }
}
