import { Component, OnInit } from '@angular/core';

import { DataService } from '../../services/data.service';
import { FilterService } from '../../services/filter.service';
import { Property } from '../../models/property';

@Component({
  moduleId: module.id,
  selector: 'my-property-viewer',
  templateUrl: 'property-viewer.component.html',
  styleUrls: ['property-viewer.component.css'],
})

export class PropertyViewerComponent implements OnInit {
  constructor(public _dataService: DataService, public _filterService: FilterService) { }

  ngOnInit() {
  }

}
