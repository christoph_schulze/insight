import { Component, OnInit, AfterViewInit, OnDestroy, NgZone, ViewChild } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { DataService } from '../../services/data.service';
import { FilterService } from '../../services/filter.service';
import { VisibilityService } from '../../services/visibility.service';
import { Property } from '../../models/property';
import { CustomHTMLElement } from '../../utils/plotly-html';

declare var Plotly: any;

let foo: HeatMapComponent;

@Component({
  moduleId: module.id,
  selector: 'heat-map',
  templateUrl: 'heat-map.component.html',
  styleUrls: ['heat-map.component.css'],
})


export class HeatMapComponent implements OnInit, AfterViewInit {
  constructor(public _dataService: DataService, public _filterService: FilterService, private zone: NgZone, private _router: Router,
  public _dialog: MdDialog, public _visibility : VisibilityService) { }

  private inputs: string[];
  private outputs: string[];
  private score: number[][];

  private inputsVar: string[];
  private outputsVar: string[];
  private scoreVar: number[][];

  closed: boolean = false;

  ngAfterViewInit() {
    this.calculateScoreComplete();

    //this.calculateScoreVar();

    let axisTemplate = {
      showgrid: true,
      linecolor: 'grey'
    };

    let layout = {
      xaxis: axisTemplate,
      yaxis: axisTemplate,
    };


    let data = [
      {
        z: this.score,
        x: this.outputs,
        y: this.inputs,
        colorscale: 'Red',
        type: 'heatmap'
      }
    ];

    let data1 = [
      {
        z: this.scoreVar,
        x: this.outputsVar,
        y: this.inputsVar,
        colorscale: 'Red',
        type: 'heatmap'
      }
    ];

    foo = this;
    Plotly.newPlot('heatmap', data, layout);
      var plot = <CustomHTMLElement>document.getElementById('heatmap');

    //Plotly.newPlot('heatmap_var', data1, layout);
      //var plot = <CustomHTMLElement>document.getElementById('heatmap_var');


      plot.on('plotly_click', function (data) {
        let outputVar: string = data.points[0].x;
        let inputVar: string = data.points[0].y;
        //this.test(inputVar, outputVar);

        foo._dataService.onlyShowPropertiesBetween(inputVar, outputVar);
        //foo.clicked = true;
        foo._router.navigate(['/properties']);
        //this._visibility = true;
        //console.log(data);
        //console.log(foo.clicked);
        //console.log('clicked');

    });
  }


  ngOnInit() {
    //this.clicked = false;


  }

  calculateScoreVar(){
    this.scoreVar = [];
    this.inputsVar = this.getInputValuesVar();
    this.outputsVar = this.getOutputValuesVar();

    this.inputs.forEach(input => {
      let scoreLine: number[] = [];
      this.outputsVar.forEach(output => {
        scoreLine.push(0);
      });
      this.scoreVar.push(scoreLine);
    });

    this._dataService.propertyMap.forEach(property => {
      let indexOutputs = this.outputsVar.indexOf(property.rhs[0].name);

      property.lhs.forEach(variable => {
        let indexInputs = this.inputsVar.indexOf(variable.name);
        let scoreUpdate = 1.0;// / property.lhs.length;
        this.scoreVar[indexInputs][indexOutputs] = this.scoreVar[indexInputs][indexOutputs] + scoreUpdate;
      });
    });

  }

  calculateScoreComplete() {
    this.score = [];
    this.inputs = this.getInputValues();
    this.outputs = this.getOutputValues();

    this.inputs.forEach(input => {
      let scoreLine: number[] = [];
      this.outputs.forEach(output => {
        scoreLine.push(0);
      });
      this.score.push(scoreLine);
    });

    this._dataService.propertyMap.forEach(property => {
      let indexOutputs = this.outputs.indexOf(property.rhs[0].name);

      property.lhs.forEach(variable => {
        let indexInputs = this.inputs.indexOf(variable.name);
        let scoreUpdate = 1.0;// / property.lhs.length;
        this.score[indexInputs][indexOutputs] = this.score[indexInputs][indexOutputs] + scoreUpdate;
      });
    });

    //this.normalize();
  }

  normalize() {
    let max = -1;
    for (var indexInputs = 0; indexInputs < this.inputs.length; indexInputs++) {
      for (var indexOutputs = 0; indexOutputs < this.outputs.length; indexOutputs++) {
        if (max < this.score[indexInputs][indexOutputs]) {
          max = this.score[indexInputs][indexOutputs];
        }
      }
    }

    let ratio = max / 100;

    for (var indexInputs = 0; indexInputs < this.inputs.length; indexInputs++) {
      for (var indexOutputs = 0; indexOutputs < this.outputs.length; indexOutputs++) {
        this.score[indexInputs][indexOutputs] = Math.round(this.score[indexInputs][indexOutputs] / ratio);
      }
    }
  }

  getOutputValues(): string[] {
    let results: string[] = [];
    this._dataService.variableMap.forEach(variable => {
      if (!variable.isLHS) {
        results.push(variable.name);
      }
    });
    return results;
  }

  getInputValues(): string[] {
    let results: string[] = [];
    this._dataService.variableMap.forEach(variable => {
      if (variable.isLHS) {
        results.push(variable.name);
      }
    });
    return results;
  }

  getOutputValuesVar(): string[] {
    let results: string[] = [];
    this._dataService.variableMap.forEach(variable => {
      if (!variable.isLHS) {
        variable.valuesMap.forEach(value => {
          results.push(variable.name + ':' + value.value);
        });

      }
    });
    return results;
  }

  getInputValuesVar(): string[] {
    let results: string[] = [];
    this._dataService.variableMap.forEach(variable => {
      if (variable.isLHS) {
        variable.valuesMap.forEach(value => {
          results.push(variable.name + ':' + value.value);
        });

      }
    });
    return results;
  }

  getLHS(): string {
    let result = '';
    this._dataService.propertyMap.forEach(property => {
      property.lhs.forEach(nameValue => {
        result += nameValue.name + '_' + nameValue.value + ' ';
      });
    });
    return result;
  }

  getRHS(): string {
    let result = '';
    this._dataService.propertyMap.forEach(property => {
      property.rhs.forEach(nameValue => {
        result += nameValue.name + '_' + nameValue.value + ' ';
      });
    });
    return result;
  }

  getFiles(property: Property): string {
    let result = '';

    for (let index = 0; index < property.statistics.fileIds.length; index++) {
      let file = property.statistics.fileIds[index];

      if (index === 0) {
        result += file;
      } else {
        result += + '\n' + file;
      }
    }

    return result;
  }

}

@Component({
  selector: 'property-dialog',
  templateUrl: './property-dialog.html',
  styleUrls: ['property-dialog.css']
})
export class PropertyDialog {
  constructor(public dialogRef: MdDialogRef<PropertyDialog>, public _visibility : VisibilityService) {}
}
