import { Component, OnInit, Input } from '@angular/core';

import { DataService } from '../../services/data.service';
import { FilterService } from '../../services/filter.service';
import {ComparisonService} from '../../services/comparison.service';
import { Property } from '../../models/property';

@Component({
  moduleId: module.id,
  selector: 'my-diff-property',
  templateUrl: 'diff-property.component.html',
  styleUrls: ['diff-property.component.css'],
})

export class DiffPropertyComponent implements OnInit {
  @Input() whichToShow: string;

  constructor(public _dataService: DataService, public _filterService: FilterService, public _comparisonService: ComparisonService) { }

  ngOnInit() {

  }

}
